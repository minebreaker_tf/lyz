/// <reference path="../typings/main.d.ts" />

'use strict';

const electron = require('electron');
const app: Electron.App = electron.app;
const BrowserWindow: typeof Electron.BrowserWindow = electron.BrowserWindow;

var mainWindow: Electron.BrowserWindow;

app.on('ready', function () {
    mainWindow = new BrowserWindow({ width:500, height: 900 });

    // Open DevTools for debugging; Comment out when release
    // mainWindow.webContents.openDevTools();

    // disable menu bar
    mainWindow.setMenu(null);

    mainWindow.loadURL('file://' + __dirname + '/index.htm');
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
});

app.on('window-all-closed', function () {
    app.quit();
});
