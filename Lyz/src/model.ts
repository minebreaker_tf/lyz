/// <reference path="../typings/main.d.ts" />

'use strict';

declare var angular:angular.IAngularStatic;

const remote = require('remote');
const fs = remote.require('fs');

angular.module('Main', [])

    .controller('MainController', ['$scope', function ($scope) {

        $scope.dataFile = './data.json';

        $scope.todoList = [];
        $scope.wipList = [];
        $scope.doneList = [];
        $scope.lists = {todo: $scope.todoList, wip: $scope.wipList, done: $scope.doneList};

        $scope.textInput = '';
        $scope.dateInput = '';
        $scope.editing = '';
        $scope.textEditing = '';
        $scope.dateEditing = '';
        $scope.notification = {title: '', content: ''};

        $scope.addTodo = function () {
            if ($scope.textInput == '' || !($scope.dateInput instanceof Date)) {
                $scope.notify('Error', 'Please fulfill all required fields');
                return;
            }

            $scope.todoList.push({
                content: $scope.textInput,
                date: {
                    year: $scope.dateInput.getFullYear(),
                    month: $scope.dateInput.getMonth() + 1,
                    day: $scope.dateInput.getDate()
                }
            });

            $scope.checkDate();
            $scope.persist();

            // clear inputs
            $scope.textInput = '';
            $scope.dateInput = '';
        };

        $scope.update = function () {
            $scope.editing.content = $scope.textEditing;
            $scope.editing.date.year = $scope.dateEditing.getFullYear();
            $scope.editing.date.month = $scope.dateEditing.getMonth() + 1;
            $scope.editing.date.day = $scope.dateEditing.getDate();

            $scope.checkDate();
            $scope.persist();

            // clear inputs
            $scope.textEditing = '';
            $scope.dateEditing = '';

            $('#edit').modal('hide');
        };

        $scope.remove = function (typeOfList, work) {
            $scope.lists[typeOfList].splice($scope.lists[typeOfList].indexOf(work), 1);
            $scope.persist();
        };

        $scope.move = function (from, to, work) {
            $scope.remove(from, work);
            // remove angular hash key from element
            work.$$hashKey = null;
            $scope.lists[to].push(work);

            $scope.persist();
        };

        $scope.toggle = function (work) {
            if ($scope.editing == work) {
                $scope.editing = ''
            } else {
                $scope.editing = work;
            }
        };

        $scope.load = function () {
            var dataString:string;
            try {
                dataString = fs.readFileSync($scope.dataFile, 'utf8');

                // clear lists if read correctly
                $scope.todoList.splice(0, $scope.todoList.length);
                $scope.wipList.splice(0, $scope.wipList.length);
                $scope.doneList.splice(0, $scope.doneList.length);
            } catch (e) {
                console.log(e);
                dataString = '{"todo": [], "wip": [], "done": []}';
            }
            console.log(dataString);

            const data = JSON.parse(dataString);
            data.todo.forEach(element => $scope.todoList.push(element));
            data.wip.forEach(element => $scope.wipList.push(element));
            data.done.forEach(element => $scope.doneList.push(element));

            $scope.checkDate();
        };

        $scope.persist = function () {
            const json = {
                todo: $scope.todoList,
                wip: $scope.wipList,
                done: $scope.doneList
            };
            const dataString:string = angular.toJson(json);
            fs.writeFile($scope.dataFile, dataString, (e) => {
                if (e) {
                    $scope.notify("Error", "Something has occurred while saving: " + e);
                }
            });
            console.log(dataString);
        };

        $scope.checkDate = function () {
            const today:Date = new Date();

            function checkEach(element) {
                const day:Date = new Date(element.date.year, element.date.month - 1, element.date.day);
                element.style = day <= today;
            };

            $scope.todoList.forEach(element => checkEach(element));
            $scope.wipList.forEach(element => checkEach(element));
            $scope.doneList.forEach(element => checkEach(element));
        };

        $scope.notify = function (title:String, content:String) {
            $scope.notification.title = title;
            $scope.notification.content = content;
            $('#notify').modal('show');
        };

        // initialization
        $scope.load();

    }]);
